package challengeEldar;

import java.time.LocalDate;
import java.util.ArrayList;

public class Main {
	
	private static ArrayList <Tarjeta> tarjetas;
	private static ArrayList <Operacion> operaciones;
	
	public static void ingresarTarjeta(String marca, int numeroTarjeta, String cardHolder, LocalDate fechaVencimiento) 
	{
		Tarjeta tarjeta = crearTarjetaCorrespondiente(marca, numeroTarjeta, cardHolder, fechaVencimiento);
		tarjetas.add(tarjeta);
	}
	public static void visualizarDatosTarjeta(Tarjeta tarjeta) {
		System.out.println(tarjeta.toString());
	}
	public static void ingresarOperacion(Tarjeta tarjeta, int monto) 
	{
		Operacion operacion = new Operacion(tarjeta, monto);
		operaciones.add(operacion);
	}
	public static void tarjetasIguales(Tarjeta tarjeta1, Tarjeta tarjeta2) {
		if (tarjeta1.equals(tarjeta2)) {
			System.out.println("son iguales");
		}
		else {
			System.out.println("no son iguales");
		}
	}
	private static Tarjeta crearTarjetaCorrespondiente(String marca, int numeroTarjeta, String cardHolder, LocalDate fechaVencimiento) 
	{
		if(marca.equalsIgnoreCase("Visa")) {
			return new TarjetaVisa(marca, numeroTarjeta, cardHolder, fechaVencimiento);
		}
		if(marca.equalsIgnoreCase("AMEX")) {
			return new TarjetaAMEX(marca, numeroTarjeta, cardHolder, fechaVencimiento);
		}
		if(marca.equalsIgnoreCase("Naranja")) {
			return new TarjetaNaranja(marca, numeroTarjeta, cardHolder, fechaVencimiento);
		}
		else
			throw new RuntimeException("La Tarjeta ingresada no corresponde con ninguna de las disponibles");
	}
	public static void main(String[] args) 
	{
	tarjetas = new ArrayList <Tarjeta>();
	operaciones = new ArrayList <Operacion>();
	LocalDate date = LocalDate.now();
	ingresarTarjeta("Visa", 11231, "Alexis Vazquez", LocalDate.parse("2020-10-30"));
	ingresarTarjeta("Visa", 11231, "Alexis Vazquez", LocalDate.parse("2020-10-30"));
	ingresarTarjeta("AMEX", 112312, "Brian Fernandez", LocalDate.parse("2022-10-30"));
	tarjetas.get(0).tarjetaValida();
	tarjetas.get(1).tarjetaValida();
	tarjetas.get(2).tarjetaValida();
	visualizarDatosTarjeta(tarjetas.get(2));
	ingresarOperacion(tarjetas.get(0), 1500);
	ingresarOperacion(tarjetas.get(0), 950);
	ingresarOperacion(tarjetas.get(1), 950);
	ingresarOperacion(tarjetas.get(2), 750);
	operaciones.get(0).operacionValida();
	operaciones.get(1).operacionValida();
	operaciones.get(2).operacionValida();
	operaciones.get(2).tasaDeOperacion(date);
	operaciones.get(3).tasaDeOperacion(date);
	tarjetasIguales(tarjetas.get(0),tarjetas.get(1));
	tarjetasIguales(tarjetas.get(0),tarjetas.get(2));
	
	}
	

}
