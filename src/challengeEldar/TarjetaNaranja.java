package challengeEldar;

import java.time.LocalDate;

public class TarjetaNaranja extends Tarjeta{

	public TarjetaNaranja(String marca, int numeroTarjeta, String cardHolder, LocalDate FechaVencimiento) {
		super(marca, numeroTarjeta, cardHolder, FechaVencimiento);
		// TODO Auto-generated constructor stub
	}

	@Override
	double tasaPorServicio(LocalDate fecha) {
		
		return fecha.getDayOfMonth()*(0.5);				
	}
	

}


