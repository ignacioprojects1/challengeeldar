package challengeEldar;

import java.time.LocalDate;


public class TarjetaAMEX extends Tarjeta{

	public TarjetaAMEX(String marca, int numeroTarjeta, String cardHolder, LocalDate FechaVencimiento) {
		super(marca, numeroTarjeta, cardHolder, FechaVencimiento);
		// TODO Auto-generated constructor stub
	}

	@Override
	double tasaPorServicio(LocalDate fecha) {
	
		return  (fecha.getMonthValue()*(0.1));
	}

}
