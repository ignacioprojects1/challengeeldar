package challengeEldar;

import java.time.LocalDate;

public class TarjetaVisa extends Tarjeta{

	public TarjetaVisa(String marca, int numeroTarjeta, String cardHolder, LocalDate FechaVencimiento) {
		super(marca, numeroTarjeta, cardHolder, FechaVencimiento);
		// TODO Auto-generated constructor stub
	}

	@Override
	double tasaPorServicio(LocalDate fecha) {
	
		int aux = fecha.getYear() - 2000; //Suponiendo que todas las fechas con las que se trabajen sean del 2000 en adelante
		return aux / fecha.getMonthValue();
	}




}
