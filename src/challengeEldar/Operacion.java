package challengeEldar;

import java.time.LocalDate;

public class Operacion {
	private Tarjeta tarjeta;
	private int monto;
	
	public Operacion(Tarjeta tarjeta, int monto) {
		this.tarjeta = tarjeta;
		this.monto = monto;
	}
	public void operacionValida() {
		if (monto<=0) {
			throw new RuntimeException("El monto tiene que ser mayor a cero");
		}
		if (monto<1000) {
			System.out.println("La operacion es valida");
			return;
		}
		System.out.println("La operacion es invalida");
	}
	public void tasaDeOperacion(LocalDate fecha) {
		if (monto<=0)
		{
			throw new RuntimeException("El monto tiene que ser mayor a cero");
		}
		
		if (monto>1000)
		{
			throw new RuntimeException("El monto supera lo permitido");
		}
		double tasaServicioTarjeta = tarjeta.tasaPorServicio(fecha);
		String marcaTarjeta = tarjeta.getMarca();
		System.out.println("La marca de la tarjeta utilizada es " + marcaTarjeta + " y el importe total contando la tasa de servicio es de " + (monto+(tasaServicioTarjeta*monto)/100));
		
	}
	public Tarjeta getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Tarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	public int getMonto() {
		return monto;
	}

	public void setMonto(int monto) {
		this.monto = monto;
	}
	
	
	

}
