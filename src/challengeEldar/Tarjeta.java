package challengeEldar;

import java.time.LocalDate;

public abstract class Tarjeta {
	private String marca;
	private int  numeroTarjeta;
	private String cardHolder;
	private LocalDate fechaVencimiento;
	
	public Tarjeta(String marca, int numeroTarjeta, String cardHolder, LocalDate fechaVencimiento) {
		this.marca = marca;
		this.numeroTarjeta = numeroTarjeta;
		this.cardHolder = cardHolder;
		this.fechaVencimiento = fechaVencimiento;
	}
	
	public void tarjetaValida() {
		LocalDate fechaActual = LocalDate.now();
		if (fechaVencimiento.isBefore(fechaActual)){
			System.out.println("La tarjeta es invalida");
			return;
		}
		System.out.println("La tarjeta es valida");
	}
	public String toString() {
	        return "Marca de la tarjeta: " + marca + "\nNumero de tarjeta: " + numeroTarjeta + "\nCardHolder: " + cardHolder + "\nFecha de vencimiento:"
	        		+ fechaVencimiento ;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		final Tarjeta other = (Tarjeta)obj;
		if(marca.equals(other.marca) && numeroTarjeta == other.numeroTarjeta && cardHolder.equals(other.cardHolder) && fechaVencimiento.equals(other.fechaVencimiento)) {
			return true;
		}
		return false;
		
		
	}
	abstract double tasaPorServicio(LocalDate fecha);
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(int numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}


}
